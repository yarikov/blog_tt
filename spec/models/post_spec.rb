require 'rails_helper'

RSpec.describe Post, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:comments) }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:body) }

  describe 'callbacks' do
    subject { build(:post) }

    it 'triggers #set_published_at on save' do
      expect(subject).to receive(:set_published_at)
      subject.save
    end
  end
end
