class UsersController < ApplicationController
  def edit
  end

  def update
    current_user.update(user_params)
    current_user.avatar =
      current_user.avatar.retrieve_from_store!(current_user.avatar_url)
    respond_with current_user, location: -> { edit_user_path(current_user) }
  end

  private

  def user_params
    params.require(:user).permit(:avatar)
  end
end
