class ReportQuery
  attr_reader :start_date, :end_date

  def self.call(report)
    new(report).call
  end

  def initialize(report)
    @start_date = quote(report.start_date)
    @end_date   = quote(report.end_date)
  end

  def call
    ActiveRecord::Base.connection.execute(query)
  end

  private

  def quote(date)
    ActiveRecord::Base.connection.quote(date)
  end

  def query
    <<-SQL
      WITH posts_between_dates AS (
        SELECT user_id, COUNT(user_id) posts_count
        FROM posts
        WHERE published_at
          BETWEEN #{start_date} AND #{end_date}
        GROUP BY user_id
      ), comments_between_dates AS (
        SELECT user_id, COUNT(user_id) comments_count
        FROM comments
        WHERE published_at
          BETWEEN #{start_date} AND #{end_date}
        GROUP BY user_id
      )
      SELECT u.nickname, u.email,
        COALESCE(p.posts_count, 0) posts_count,
        COALESCE(c.comments_count, 0) comments_count
      FROM users u
        LEFT OUTER JOIN posts_between_dates p ON u.id = p.user_id
        LEFT OUTER JOIN comments_between_dates c ON u.id = c.user_id
      WHERE p.posts_count IS NOT NULL OR c.comments_count IS NOT NULL
      GROUP BY u.nickname, u.email, p.posts_count, c.comments_count
      ORDER BY
        COALESCE(p.posts_count, 0) + COALESCE(c.comments_count, 0) / 10.0 DESC,
        p.posts_count, u.nickname;
    SQL
  end
end
