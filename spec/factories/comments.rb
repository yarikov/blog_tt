FactoryGirl.define do
  factory :comment do
    user
    post
    body { Faker::Lorem.paragraph }
    published_at Date.current.in_time_zone
  end
end
