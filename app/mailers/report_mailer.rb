class ReportMailer < ApplicationMailer
  def send_report(report)
    @report = ReportQuery.call(report)

    mail to: report.email
  end
end
