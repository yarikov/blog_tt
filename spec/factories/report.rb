FactoryGirl.define do
  factory :report do
    start_date 2.days.ago
    end_date   Time.zone.now
    email 'user@example.com'
  end
end
