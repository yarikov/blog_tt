require 'application_responder'

class Api::V1::BaseController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  self.responder = ApplicationResponder
  respond_to :json
end
