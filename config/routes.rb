Rails.application.routes.draw do
  devise_for :users

  root to: 'pages#index'

  resources :users, only: [:edit, :update]

  namespace :api do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth'

      resources :posts, only: [:index, :show, :create]

      post 'reports/by_author'
    end
  end
end
