class User < ActiveRecord::Base
  has_many :posts
  has_many :comments

  mount_uploader :avatar, AvatarUploader

  # Include default devise modules.
  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  validates :nickname, presence: true, uniqueness: true
  validates_integrity_of  :avatar
  validates_processing_of :avatar
  validate :avatar_size

  private

  def avatar_size
    return unless avatar.size > 3.megabytes
    errors[:avatar] << 'Avatar size should be less than 3MB'
  end
end
