FactoryGirl.define do
  factory :post do
    user
    title { Faker::Lorem.sentence }
    body  { Faker::Lorem.paragraph }
    published_at Date.current.in_time_zone
  end

  factory :invalid_post, class: 'Post' do
    title nil
    body  nil
  end
end
