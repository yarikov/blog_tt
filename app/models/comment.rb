class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  validates :body, :published_at, presence: true
end
