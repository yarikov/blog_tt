class Api::V1::ReportsController < Api::V1::BaseController
  def by_author
    report = Report.new(report_params)
    if report.save
      ReportMailer.send_report(report).deliver_later
      render json: { message: 'Report generation started' }, status: 201
    else
      render json: { errors: report.errors }, status: 422
    end
  end

  private

  def report_params
    params.require(:report).permit(:start_date, :end_date, :email)
  end
end
