class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.text :body
      t.references :post, foreign_key: true
      t.references :user, foreign_key: true
      t.datetime :published_at

      t.timestamps
    end
    add_index :comments, :published_at
  end
end
