class Api::V1::PostsController < Api::V1::BaseController
  before_action :authenticate_user!, only: :create

  def index
    posts = Post.includes(:user)
                .order(published_at: :desc)
                .page(params[:page]).per(params[:per_page])

    headers.merge!('Pages' => posts.total_pages, 'Posts' => posts.total_count)
    respond_with :api, :v1, posts
  end

  def show
    respond_with :api, :v1, Post.find(params[:id])
  end

  def create
    respond_with :api, :v1, current_user.posts.create(post_params)
  end

  private

  def post_params
    params.require(:post).permit(:title, :body, :published_at)
  end
end
