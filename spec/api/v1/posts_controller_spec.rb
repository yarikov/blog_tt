require 'rails_helper'

describe Api::V1::PostsController do
  describe 'GET /index' do
    let!(:posts) { create_list(:post, 3) }
    let!(:post)  { create(:post, published_at: 3.days.ago) }

    before { get '/api/v1/posts.json?page=2&per_page=2' }

    it 'responds with a 200 status code' do
      expect(response).to have_http_status(200)
    end

    it 'responds with the headers' do
      expect(headers['Pages']).to eq 2
      expect(headers['Posts']).to eq 4
    end

    context 'responds with JSON' do
      it 'contains list of posts' do
        expect(response.body).to have_json_size(2).at_path('')
      end

      context 'sorts posts by published_at' do
        %w(id title body published_at).each do |attr|
          it "contains #{attr}" do
            expect(response.body)
              .to be_json_eql(post.send(attr.to_sym).to_json)
              .at_path("1/#{attr}")
          end
        end

        it 'contains author_nickname' do
          expect(response.body)
            .to be_json_eql(post.user.nickname.to_json)
            .at_path('1/author_nickname')
        end
      end
    end
  end

  describe 'GET /show' do
    let(:post) { create(:post) }

    before { get "/api/v1/posts/#{post.id}.json" }

    it 'responds with a 200 status code' do
      expect(response).to have_http_status(200)
    end

    context 'responds with JSON' do
      %w(id title body published_at).each do |attr|
        it "contains #{attr}" do
          expect(response.body)
            .to be_json_eql(post.send(attr.to_sym).to_json)
            .at_path(attr.to_s)
        end
      end

      it 'contains author_nickname' do
        expect(response.body)
          .to be_json_eql(post.user.nickname.to_json)
          .at_path('author_nickname')
      end
    end
  end

  describe 'POST /create' do
    let(:author)          { create(:user) }
    let(:post_attributes) { attributes_for(:post) }

    context 'with auth token' do
      context 'when valid attributes' do
        before do
          auth_token = author.create_new_auth_token
          post '/api/v1/posts.json',
               params: { post: post_attributes },
               headers: auth_token
        end

        it 'responds with a 201 status code' do
          expect(response).to have_http_status(201)
        end

        context 'responds with JSON' do
          it 'contains id' do
            expect(response.body).to have_json_path('id')
          end

          it 'contains author_nickname' do
            expect(response.body)
              .to be_json_eql(author.nickname.to_json)
              .at_path('author_nickname')
          end

          %w(title body published_at).each do |attr|
            it "contains #{attr}" do
              expect(response.body)
                .to be_json_eql(post_attributes[attr.to_sym].to_json)
                .at_path(attr)
            end
          end
        end
      end

      context 'when invalid attributes' do
        before do
          auth_token = author.create_new_auth_token
          post '/api/v1/posts.json',
               params: { post: attributes_for(:invalid_post) },
               headers: auth_token
        end

        it 'responds with a 422 status code' do
          expect(response).to have_http_status(422)
        end

        context 'responds with JSON' do
          it 'contains list of errors' do
            expect(response.body).to have_json_size(2).at_path('errors')
          end
        end
      end
    end

    context 'without auth token' do
      before { post '/api/v1/posts.json', params: { post: post_attributes } }

      it 'responds with a 401 status code' do
        expect(response).to have_http_status(401)
      end
    end
  end
end
