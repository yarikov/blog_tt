FactoryGirl.define do
  factory :user, class: User do
    email    { Faker::Internet.email }
    nickname { Faker::Internet.user_name }
    password 'password'
    password_confirmation 'password'
  end
end
