require 'rails_helper'

RSpec.describe ReportQuery do
  describe '.call' do
    let(:report) do
      build(:report, start_date: 2.days.ago, end_date: Time.zone.now)
    end

    let!(:user1) { create(:user) }
    let!(:user2) { create(:user) }

    let!(:post1) { create(:post, published_at: 1.day.ago, user: user1) }
    let!(:post2) { create(:post, published_at: 3.days.ago, user: user1) }

    let!(:comment1) do
      create(:comment, published_at: 1.day.ago, user: user2, post: post1)
    end
    let!(:comment2) do
      create(:comment, published_at: 3.days.ago, user: user2, post: post1)
    end

    subject { described_class.call(report) }

    it 'returns stats' do
      expect(subject).to match(
        [{
          'nickname'       => user1.nickname,
          'email'          => user1.email,
          'posts_count'    => 1,
          'comments_count' => 0
        },
         {
           'nickname'       => user2.nickname,
           'email'          => user2.email,
           'posts_count'    => 0,
           'comments_count' => 1
         }]
      )
    end
  end
end
