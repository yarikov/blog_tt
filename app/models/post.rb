class Post < ApplicationRecord
  belongs_to :user
  has_many :comments

  validates :title, :body, presence: true

  before_save :set_published_at

  private

  def set_published_at
    self.published_at ||= Time.zone.now
  end
end
